﻿$(document).ready(function () {

    /*Redirect if not Logged In*/
    if (localStorage.getItem('accessToken') == null) {
        window.location.href = "Login.html";
    }

    /*Log out of App*/
    $('#btnLogOut').click(function () {
        localStorage.removeItem('accessToken');
        window.location.href = "Login.html";
    });

    $('#closeprofile').click(function () {
        document.getElementById('userprofile').style.display = 'none';
        document.getElementById("username").innerHTML = "";
        document.getElementById("useremail").innerHTML = "";
        document.getElementById("userbirthday").innerHTML = "";
        document.getElementById("usergender").innerHTML = "";
    });
    
    $('#closefeed').click(function () {
        document.getElementById('userfeed').style.display = 'none';
    });

    $('#closelikes').click(function () {
        document.getElementById('userlikes').style.display = 'none';
    });

    $('#closePhotos').click(function () {
        document.getElementById('userPhotos').style.display = 'none';
    });

    $('#closeposts').click(function () {
        document.getElementById('userposts').style.display = 'none';
    });

    $('#closetimeline').click(function () {
        document.getElementById('usertweets').style.display = 'none';
    });

    $('#closefavetweets').click(function () {
        document.getElementById('userfavtweets').style.display = 'none';
    });

    $('#closefriendlist').click(function () {
        document.getElementById('userfriends').style.display = 'none';
    });
    
    $('#closefollowers').click(function () {
        document.getElementById('userfollowers').style.display = 'none';
    });

    $('#closesearch').click(function () {
        document.getElementById('search').style.display = 'none';
    });

    $('#closeHistory').click(function () {
        document.getElementById('searchHisotry').style.display = 'none';
    }); 

    $('#closepage').click(function () {
        document.getElementById('profilePosts').style.display = 'none';
    }); 

    
    
});


/*Facebook Button*/
window.fbAsyncInit = function () {
    FB.init({
        appId: '266810171183187',
        cookie: true,
        xfbml: true,
        version: 'v7.0'
    });


    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });

};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

/*Facebook*/
function statusChangeCallback(response) {
    /*Succesful Login*/
    if (response.status === 'connected') {
        var url = ""; //http://keaneschembri02-001-site1.etempurl.com

        /*Token*/
        var facebookToken = response.authResponse.accessToken;

        console.log(response.access_token);

        $.ajax({
            url: url + '/api/facebook/getFullName?accesstoken=' + facebookToken,
            method: 'GET',
            data: facebookToken,
            success: function (data) {
                document.getElementById("welcomeMsg").innerHTML = "Welcome to Facebook " + data;
                document.getElementById('profile').style.display = 'block';
                document.getElementById('fb-logout').style.display = 'block';
                document.getElementById('getFeed').style.display = 'block';
                document.getElementById('getLikes').style.display = 'block';
                document.getElementById('getPhotos').style.display = 'block';
                document.getElementById('getPosts').style.display = 'block';
                document.getElementById('postCmntBtn').style.display = 'block';
                setElements(true);
            },
            error: function (e) {
                console.log("error " + e);
            }
        });

        /*Display Page div*/
        $('#postCmntBtn').click(function () {
            $.ajax({
                url: url + '/api/facebook/UserId?accesstoken=' + facebookToken,
                method: 'GET',
                data: facebookToken,
                success: function (data) {
                    document.getElementById('profilePosts').style.display = 'block';
                    GetPostIdAPI(data);
                },
                error: function (e) {
                    console.log("error " + e);
                }
            });
        });

        /*Display Pages Posts*/
        function GetPostIdAPI(data) {
            sessionStorage.setItem("PostToken", data[0].access_token);
            $.ajax({
                url: url + '/api/facebook/PostId',
                data: {
                    id: data[0].id,
                    accessToken: data[0].access_token
                },
                method: 'GET',
                header: {
                    'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
                },
                contentType: 'application/json',
                success: function (response) {
                    var output = '<h3>Posts</h3>';

                    if (response[0]['message']) {
                        var inputMsgID = 'getMessage'+[0];
                        var responseID = response[0]['id'];

                        output = '<li class="list-group-item bg-dark">' + response[0]['message'] + '</li>';
                        output += '<input type="text" class="form-control" id=' + inputMsgID + '>';
                        output += '<button class="postBtnClick btn btn-primary" id="'+ responseID + '">Comment</button>';

                        document.getElementById('postsBody').innerHTML = output;
                    }
                },
                error: function (e) {
                    console.log("error" + e);
                }
            });
        }

        /*comment status*/
        function comment(response, txtMsg) {
            let id = response;
            let value = sessionStorage.getItem("PostToken");
            let msg = txtMsg;

            $.ajax({
                url: url + '/api/facebook/PostComment?id=' + id + '&message=' + msg + '&access_token=' + value + '&accesstoken=' + facebookToken,
                method: 'POST',
                header: {
                    'Authorization': 'Bearer ' + localStorage.getItem('accessToken')
                },
                success: function () {
                    alert("Comment Posted");
                },
                error: function (e) {
                    console.log("error" + e);
                }
            });
        }

        /*Call function comment*/
        $(document).on("click",".postBtnClick",function () {
            comment($(this).attr("id"), $(this).siblings("input").val());
        });  

        /*Profile Details*/
        $('#getProfile').click(function () {

            document.getElementById("username").innerHTML = "";
            document.getElementById("useremail").innerHTML = "";
            document.getElementById("userbirthday").innerHTML = "";
            document.getElementById("usergender").innerHTML = "";
            
            var name = $('#name').prop("checked");
            var email = $('#email').prop("checked");
            var birthday = $('#birthday').prop("checked");
            var gender = $('#gender').prop("checked");
            
            $.ajax({
                url: url + '/api/facebook/getProfileReads?accesstoken=' + facebookToken + '&name=' + name + '&email=' + email + '&birthday=' + birthday + '&gender=' + gender,
                method: 'GET',
                data: facebookToken,
                success: function (data) {
                    document.getElementById('userprofile').style.display = 'block';

                    if (name) {
                        document.getElementById("username").innerHTML = "Name: " + data[0];
                        $('#name').prop('checked', false);
                    }
                    if (email) {
                        document.getElementById("useremail").innerHTML = "Email: " + data[1];
                        $('#email').prop('checked', false);
                    }
                    if (birthday) {
                        document.getElementById("userbirthday").innerHTML = "Birthday: " + data[2];
                        $('#birthday').prop('checked', false);
                    }
                    if (gender) {
                        document.getElementById("usergender").innerHTML = "Gender: " + data[3];
                        $('#gender').prop('checked', false);
                    }

                },
                error: function (e) {
                    console.log("error " + e);
                }
            });
        });

        /*Feed*/
        $('#getFeed').click(function () {
            $.ajax({
                url: url + '/api/facebook/getProfileFeed?accesstoken=' + facebookToken,
                method: 'GET',
                data: facebookToken,
                success: function (data) {
                    document.getElementById('userfeed').style.display = 'block';

                    if (data[0].message != null) {
                        document.getElementById("message").innerHTML = data[0].message;
                        document.getElementById("createdtime").innerHTML = data[0].created_time;
                    }
                    if (data[1].message != null) {
                        $('#createdtime').append('<hr><p class="card-text">' + data[1].message + '</p>' + '<p class="card-text">' + data[1].created_time + '</p>');
                    }
                    if (data[2].message != null) {
                        $('#createdtime').append('<hr><p class="card-text">' + data[2].message + '</p>' + '<p class="card-text">' + data[2].created_time + '</p>');
                    }

                },
                error: function (e) {
                    console.log("error " + e);
                }
            });
        });

        /*Likes*/
        $('#getLikes').click(function () {
            $.ajax({
                url: url + '/api/facebook/getProfileLikes?accesstoken=' + facebookToken,
                method: 'GET',
                data: facebookToken,
                success: function (data) {
                    document.getElementById('userlikes').style.display = 'block';
                    
                    if (data[0].name != null) {
                        document.getElementById("likedPageName").innerHTML = data[0].name;
                    }
                    if (data[1].name != null) {
                        $('#likedPageName').append('<hr><p class="card-text">' + data[1].name + '</p>');
                    }
                    if (data[2].name != null) {
                        $('#likedPageName').append('<hr><p class="card-text">' + data[2].name + '</p>');
                    }

                },
                error: function (e) {
                    console.log("error " + e);
                }
            });
        });

        /*Photos*/
        $('#getPhotos').click(function () {
            $.ajax({
                url: url + '/api/facebook/getProfilePhotos?accesstoken=' + facebookToken,
                method: 'GET',
                data: facebookToken,
                success: function (data) {
                    document.getElementById('userPhotos').style.display = 'block';
                    
                    if (data[0].picture != null) {
                        document.getElementById("photoURL").innerHTML = ('<img src="' + data[0].picture + '">');
                    }
                    if (data[1].picture != null) {
                        $('#photoURL').append('<img src="' + data[1].picture + '">');
                    }
                    if (data[2].picture != null) {
                        $('#photoURL').append('<img src="' + data[2].picture + '">');
                    }

                },
                error: function (e) {
                    console.log("error " + e);
                }
            });
        });

        /*Posts*/
        $('#getPosts').click(function () {
            $.ajax({
                url: url + '/api/facebook/getProfilePosts?accesstoken=' + facebookToken,
                method: 'GET',
                data: facebookToken,
                success: function (data) {
                    document.getElementById('userposts').style.display = 'block';

                    if (data[0].message != null) {
                        document.getElementById("postmessage").innerHTML = data[0].message;
                        document.getElementById("postcreatedtime").innerHTML = data[0].created_time;
                    }
                    if (data[1].message != null) {
                        $('#postcreatedtime').append('<hr><p class="card-text">' + data[1].message + '</p>' + '<p class="card-text">' + data[1].created_time + '</p>');
                    }
                    if (data[2].message != null) {
                        $('#postcreatedtime').append('<hr><p class="card-text">' + data[2].message + '</p>' + '<p class="card-text">' + data[2].created_time + '</p>');
                    }

                },
                error: function (e) {
                    console.log("error " + e);
                }
            });
        });

        
    }
    else {
        setElements(false);
    }
}

/*Check Login Status*/
function checkLoginState() {
    FB.getLoginStatus(function (response) {
        statusChangeCallback(response);
    });
}

/*Set Logged in elements if Logged in*/
function setElements(isLoggedIn) {
    if (isLoggedIn) {
        document.getElementById('fb-btn').style.display = 'none';
        document.getElementById('getProfile').style.display = 'block'; 
        document.getElementById('profileDiv').style.display = 'block';
    }
    else {
        document.getElementById('fb-btn').style.display = 'block';
        document.getElementById('profile').style.display = 'none';
        document.getElementById('fb-logout').style.display = 'none';
        document.getElementById('getProfile').style.display = 'none';
        document.getElementById('profileDiv').style.display = 'none';
        document.getElementById('getFeed').style.display = 'none';
        document.getElementById('userfeed').style.display = 'none';
        document.getElementById('userlikes').style.display = 'none';
        document.getElementById('userprofile').style.display = 'none';
        document.getElementById('userPhotos').style.display = 'none';
        document.getElementById('getPhotos').style.display = 'none';
        document.getElementById('getPosts').style.display = 'none';
        document.getElementById('postCmntBtn').style.display = 'none';
        document.getElementById('getLikes').style.display = 'none';
        document.getElementById('userposts').style.display = 'none';

    }
}

/*Facebook Log out Button*/
function logOut() {
    FB.logout(function (response) {
        setElements(false);
    });
}

/*Twitter*/
$('#twitterlogin').click(function () {
    var provider = new firebase.auth.TwitterAuthProvider();
    firebase.auth().useDeviceLanguage();
    firebase.auth().signInWithPopup(provider).then(function (result) {

        var url = ""; //http://keaneschembri02-001-site1.etempurl.com

        /*Logged in Successfull*/
        if (result.credential.accessToken != null) {
            document.getElementById('twitterlogin').style.display = 'none';
            document.getElementById('twitterProfile').style.display = 'block';
            document.getElementById('tw-logout').style.display = 'block';
            document.getElementById('getFavTweets').style.display = 'block';
            document.getElementById('getTweets').style.display = 'block';
            document.getElementById('getFriends').style.display = 'block';
            document.getElementById('getFollowers').style.display = 'block';
            document.getElementById('searchQuery').style.display = 'block';
            document.getElementById('searchBtn').style.display = 'block';
            document.getElementById('searchHistoryBtn').style.display = 'block';
            document.getElementById('tweetQuery').style.display = 'block';
            document.getElementById('postTweetBtn').style.display = 'block';
            document.getElementById("twitterWelcomeMsg").innerHTML = "Welcome to Twitter " + result.additionalUserInfo.username;

            /*Tokens*/
            var twitterToken = result.credential.accessToken;
            var twitterSecretToken = result.credential.secret;
            var twitterUsername = result.additionalUserInfo.username;

            /*Timeline Tweets*/
            $('#getTweets').click(function () {
                $.ajax({
                    url: url + '/api/twitter/getTweets?screenName=' + twitterUsername + '&count=5&AccessToken=' + twitterToken + '&AccessTokenSecret=' + twitterSecretToken,
                    method: 'GET',
                    success: function (data) {
                        document.getElementById('usertweets').style.display = 'block';

                        if (data[0] != null) {
                            document.getElementById("timelineTweet").innerHTML = data[0];
                        }
                        if (data[1] != null) {
                            $('#timelineTweet').append('<hr><p class="card-text">' + data[1] + '</p>');
                        }
                        if (data[2] != null) {
                            $('#timelineTweet').append('<hr><p class="card-text">' + data[2] + '</p>');
                        }
                        if (data[3] != null) {
                            $('#timelineTweet').append('<hr><p class="card-text">' + data[3] + '</p>');
                        }
                        if (data[4] != null) {
                            $('#timelineTweet').append('<hr><p class="card-text">' + data[4] + '</p>');
                        }
                    },
                    error: function (e) {
                        console.log("error " + e);
                    }
                });
            });

            /*Favorite Tweets*/
            $('#getFavTweets').click(function () {
                $.ajax({
                    url: url + '/api/twitter/getFavTweets?screenName=' + twitterUsername + '&count=2&AccessToken=' + twitterToken + '&AccessTokenSecret=' + twitterSecretToken,
                    method: 'GET',
                    success: function (data) {
                        document.getElementById('userfavtweets').style.display = 'block';

                        if (data[0] != null) {
                            document.getElementById("favTweet").innerHTML = data[0];
                        }
                        if (data[1] != null) {
                            $('#favTweet').append('<hr><p class="card-text">' + data[1] + '</p>');
                        }
                    },
                    error: function (e) {
                        console.log("error " + e);
                    }
                });
            });

            /*Friends / Following*/
            $('#getFriends').click(function () {
                $.ajax({
                    url: url + '/api/twitter/getFriends?screenName=' + twitterUsername + '&count=2&AccessToken=' + twitterToken + '&AccessTokenSecret=' + twitterSecretToken,
                    method: 'GET',
                    success: function (data) {
                        document.getElementById('userfriends').style.display = 'block';

                        if (data[0] != null) {
                            document.getElementById("userfriendlist").innerHTML = data[0].name;
                        }
                        if (data[1] != null) {
                            $('#userfriendlist').append('<hr><p class="card-text">' + data[1].name + '</p>');
                        }
                    },
                    error: function (e) {
                        console.log("error " + e);
                    }
                });
            });

            /*Following*/
            $('#getFollowers').click(function () {
                $.ajax({
                    url: url + '/api/twitter/getFollowers?screenName=' + twitterUsername + '&count=2&AccessToken=' + twitterToken + '&AccessTokenSecret=' + twitterSecretToken,
                    method: 'GET',
                    success: function (data) {
                        document.getElementById('userfollowers').style.display = 'block';

                        if (data[0] != null) {
                            document.getElementById("userfollowerslist").innerHTML = data[0].name;
                        }
                        if (data[1] != null) {
                            $('#userfollowerslist').append('<hr><p class="card-text">' + data[1].name + '</p>');
                        }
                    },
                    error: function (e) {
                        console.log("error " + e);
                    }
                });
            });

            /*Search for Tweets*/
            $('#searchBtn').click(function () {
                
                var searchquery = $('#searchQuery').val();

                $.ajax({
                    url: url + '/api/twitter/getSearch?screenName=' + twitterUsername + '&q=' + searchquery + '&count=2&AccessToken=' + twitterToken + '&AccessTokenSecret=' + twitterSecretToken,
                    method: 'GET',
                    success: function (data) {
                        document.getElementById('search').style.display = 'block';

                        if (data[0] != null) {
                            document.getElementById("searchlist").innerHTML = data[0].text;
                        }
                        if (data[1] != null) {
                            $('#searchlist').append('<hr><p class="card-text">' + data[1].text + '</p>');
                        }
                    },
                    error: function (e) {
                        console.log("error " + e);
                    }
                });
            });

            /*Search History*/
            $('#searchHistoryBtn').click(function () {

                $.ajax({
                    url: url + '/api/twitter/getSearchHisotry',
                    method: 'GET',
                    success: function (data) {
                        document.getElementById('searchHisotry').style.display = 'block';
                        
                        if (data[0] != null) {
                            document.getElementById("historyList").innerHTML = data[0].searchQuery;
                        }
                        if (data[1] != null) {
                            $('#historyList').append('<hr><p class="card-text">' + data[1].searchQuery + '</p>');
                        }
                        if (data[2] != null) {
                            document.getElementById("historyList").innerHTML = data[2].searchQuery;
                        }
                        if (data[3] != null) {
                            $('#historyList').append('<hr><p class="card-text">' + data[3].searchQuery + '</p>');
                        }
                    },
                    error: function (e) {
                        console.log("error " + e);
                    }
                });
            });

            /*Publish Tweet*/
            $('#postTweetBtn').click(function () {

                var tweetquery = $('#tweetQuery').val();

                $.ajax({
                    url: url + '/api/twitter/Tweet?tweetQuery=' + tweetquery + '&screenName=' + twitterUsername + '&count=1&AccessToken=' + twitterToken + '&AccessTokenSecret=' + twitterSecretToken,
                    method: 'POST',
                    success: function (data) {
                        alert("Tweet Posted Successfully! Go To Timeline");
                    },
                    error: function (e) {
                        console.log("error " + e);
                    }
                });
            });
        }
    }).catch(function (error) {
        console.log(error);
    });
});