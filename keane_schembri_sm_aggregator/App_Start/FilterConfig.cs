﻿using System.Web;
using System.Web.Mvc;

namespace keane_schembri_sm_aggregator
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
