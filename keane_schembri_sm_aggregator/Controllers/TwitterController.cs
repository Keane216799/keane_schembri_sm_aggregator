﻿using keane_schembri_sm_aggregator.Endpoints;
using keane_schembri_sm_aggregator.JSONParser;
using keane_schembri_sm_aggregator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using static keane_schembri_sm_aggregator.Models.TwitterFollowingModel;
using static keane_schembri_sm_aggregator.Models.TwitterSearchModel;

namespace keane_schembri_sm_aggregator.Controllers
{
    [RoutePrefix("api/twitter")]
    public class TwitterController : ApiController
    {
        private TwitterEndpoint twitterEndpoint;

        public TwitterController()
        {
        }

        [Route("getTweets")]
        public List<string> GetTweets(string screenName, int count, string AccessToken, string AccessTokenSecret)
        {
            this.twitterEndpoint = new TwitterEndpoint(screenName, count, AccessToken, AccessTokenSecret);
            string response = twitterEndpoint.getByUserTimeline();

            List<string> twittertimeline = new List<string>();
            
            using (JsonParser<List<TwitterUserTimelineModel>> JsonParser = new JsonParser<List<TwitterUserTimelineModel>>())
            {
                List<TwitterUserTimelineModel> deserialisedtwittermodel = JsonParser.parseJson(response);
                for(int i=0;i<deserialisedtwittermodel.Count; i++)
                {
                    twittertimeline.Add(deserialisedtwittermodel[i].text);
                }
            }

            return twittertimeline;
        }

        [Route("getFavTweets")]
        public List<string> GetFavTweets(string screenName, int count, string AccessToken, string AccessTokenSecret)
        {
            this.twitterEndpoint = new TwitterEndpoint(screenName, count, AccessToken, AccessTokenSecret);
            string response = twitterEndpoint.getByFavTweet();

            List<string> favTweets = new List<string>();

            using (JsonParser<List<TwitterUserTimelineModel>> JsonParser = new JsonParser<List<TwitterUserTimelineModel>>())
            {
                List<TwitterUserTimelineModel> deserialisedtwittermodel = JsonParser.parseJson(response);
                for (int i = 0; i < deserialisedtwittermodel.Count; i++)
                {
                    favTweets.Add(deserialisedtwittermodel[i].text);
                }
            }

            return favTweets;
        }

        [Route("getFriends")]
        public List<UserData> GetFriends(string screenName, int count, string AccessToken, string AccessTokenSecret)
        {
            this.twitterEndpoint = new TwitterEndpoint(screenName, count, AccessToken, AccessTokenSecret);
            string response = twitterEndpoint.getByFriends();
            
            JsonParser<TwitterFollowingModel> JsonParser = new JsonParser<TwitterFollowingModel>();

            TwitterFollowingModel deserialisedtwittermodel = new TwitterFollowingModel();
            deserialisedtwittermodel = JsonParser.parseJson(response);

            List<UserData> twitterFriends = new List<UserData>();

            foreach (UserData feed in deserialisedtwittermodel.users)
            {
                twitterFriends.Add(feed);
            }

            return twitterFriends;
        }

        [Route("getFollowers")]
        public List<UserData> GetFollowers(string screenName, int count, string AccessToken, string AccessTokenSecret)
        {
            this.twitterEndpoint = new TwitterEndpoint(screenName, count, AccessToken, AccessTokenSecret);
            string response = twitterEndpoint.getByFollowers();

            JsonParser<TwitterFollowingModel> JsonParser = new JsonParser<TwitterFollowingModel>();

            TwitterFollowingModel deserialisedtwittermodel = new TwitterFollowingModel();
            deserialisedtwittermodel = JsonParser.parseJson(response);

            List<UserData> twitterFollowers = new List<UserData>();

            foreach (UserData feed in deserialisedtwittermodel.users)
            {
                twitterFollowers.Add(feed);
            }

            return twitterFollowers;
        }

        [Route("getSearch")]
        public List<TwitterSearchModel.search> GetSearch(string screenName, int count, string AccessToken, string AccessTokenSecret, string q)
        {
            TwitterSearchHistoryModel search = new TwitterSearchHistoryModel();
            search.searchQuery = q;
            TwitterSearchHistoryModelsController cnt = new TwitterSearchHistoryModelsController();
            cnt.Create(search);

            this.twitterEndpoint = new TwitterEndpoint(screenName, count, AccessToken, AccessTokenSecret);
            string response = twitterEndpoint.getSearch(q);

            JsonParser<TwitterSearchModel> JsonParser = new JsonParser<TwitterSearchModel>();

            TwitterSearchModel deserialisedtwittermodel = new TwitterSearchModel();
            deserialisedtwittermodel = JsonParser.parseJson(response);

            List<search> twitterSearch = new List<search>();

            foreach (search feed in deserialisedtwittermodel.statuses)
            {
                twitterSearch.Add(feed);
            }

            return twitterSearch;
        }

        [Route("getSearchHisotry")]
        public List<TwitterSearchHistoryModel> Get()
        {
            using (ApplicationDbContext entities = new ApplicationDbContext())
            {
                return entities.TwitterSearchHistoryModels.ToList();
            }
        }
        
        [Route("Tweet")]
        public async Task<IHttpActionResult> PostTweet(string screenName, int count, string AccessToken, string AccessTokenSecret, string tweetQuery)
        {
            this.twitterEndpoint = new TwitterEndpoint(screenName, count, AccessToken, AccessTokenSecret);
            string response = twitterEndpoint.postTweet(tweetQuery);
            
            return Ok();
        }
    }    
}