﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using keane_schembri_sm_aggregator.Models;

namespace keane_schembri_sm_aggregator.Controllers
{
    public class facebookProfilePreferencesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: facebookProfilePreferences
        public ActionResult Index()
        {
            return View(db.facebookProfilePreferences.ToList());
        }

        // GET: facebookProfilePreferences/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            facebookProfilePreferences facebookProfilePreferences = db.facebookProfilePreferences.Find(id);
            if (facebookProfilePreferences == null)
            {
                return HttpNotFound();
            }
            return View(facebookProfilePreferences);
        }

        // GET: facebookProfilePreferences/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: facebookProfilePreferences/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "preference_id,name,email,birthday,gender")] facebookProfilePreferences facebookProfilePreferences)
        {
            if (ModelState.IsValid)
            {
                db.facebookProfilePreferences.Add(facebookProfilePreferences);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(facebookProfilePreferences);
        }

        // GET: facebookProfilePreferences/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            facebookProfilePreferences facebookProfilePreferences = db.facebookProfilePreferences.Find(id);
            if (facebookProfilePreferences == null)
            {
                return HttpNotFound();
            }
            return View(facebookProfilePreferences);
        }

        // POST: facebookProfilePreferences/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "preference_id,name,email,birthday,gender")] facebookProfilePreferences facebookProfilePreferences)
        {
            if (ModelState.IsValid)
            {
                db.Entry(facebookProfilePreferences).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(facebookProfilePreferences);
        }

        // GET: facebookProfilePreferences/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            facebookProfilePreferences facebookProfilePreferences = db.facebookProfilePreferences.Find(id);
            if (facebookProfilePreferences == null)
            {
                return HttpNotFound();
            }
            return View(facebookProfilePreferences);
        }

        // POST: facebookProfilePreferences/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            facebookProfilePreferences facebookProfilePreferences = db.facebookProfilePreferences.Find(id);
            db.facebookProfilePreferences.Remove(facebookProfilePreferences);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
