﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace keane_schembri_sm_aggregator.Controllers
{
    class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
