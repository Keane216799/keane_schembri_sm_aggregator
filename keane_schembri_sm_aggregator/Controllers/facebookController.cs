﻿using Antlr.Runtime;
using keane_schembri_sm_aggregator.Client;
using keane_schembri_sm_aggregator.Endpoints;
using keane_schembri_sm_aggregator.JSONParser;
using keane_schembri_sm_aggregator.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using static keane_schembri_sm_aggregator.Models.facebookPageModel;
using static keane_schembri_sm_aggregator.Models.facebookPostCommentModel;

namespace keane_schembri_sm_aggregator.Controllers
{
    [RoutePrefix("api/facebook")]
    public class facebookController : ApiController
    {
        public string accesstoken { get; set; }

        protected RestClient client;
        private facebookEndpoint facebookEndpoint;

        public facebookController()
        {
            this.client = new RestClient();
        }

        [HttpGet]
        [Route("getFullName")]
        public string getCurrentReading(string accesstoken)
        {
            this.facebookEndpoint = new facebookEndpoint(accesstoken);

            client.endpoint = facebookEndpoint.getByNameEndpoint();
            string response = client.makeRequest(httpVerb.GET);

            JsonParser<facebookModel> JsonParser = new JsonParser<facebookModel>();

            facebookModel deserialisedfacebookmodel = new facebookModel();
            deserialisedfacebookmodel = JsonParser.parseJson(response);

            string name = deserialisedfacebookmodel.name;

            return name;
        }

        /*Profile Readings According to Preferences*/
        [HttpGet]
        [Route("getProfileReads")]
        public string[] getProfileReadings(string accesstoken, bool name, bool email, bool birthday, bool gender)
        {
            this.facebookEndpoint = new facebookEndpoint(accesstoken);

            client.endpoint = facebookEndpoint.getByProfileEndpoint();
            string response = client.makeRequest(httpVerb.GET);

            JsonParser<facebookModel> JsonParser = new JsonParser<facebookModel>();

            facebookModel deserialisedfacebookmodel = new facebookModel();
            deserialisedfacebookmodel = JsonParser.parseJson(response);

            string returned_name = deserialisedfacebookmodel.name;
            string returned_email = deserialisedfacebookmodel.email;
            string returned_birthday = deserialisedfacebookmodel.birthday;
            string returned_gender = deserialisedfacebookmodel.gender;

            string[] profile = { returned_name, returned_email, returned_birthday, returned_gender };

            facebookProfilePreferences profilePreference = new facebookProfilePreferences();

            profilePreference.name = name;
            profilePreference.email = email;
            profilePreference.birthday = birthday;
            profilePreference.gender = gender;

            facebookProfilePreferencesController cnt = new facebookProfilePreferencesController();
            cnt.Create(profilePreference);

            return profile;
        }

        /*Feed*/
        [HttpGet]
        [Route("getProfileFeed")]
        public List<facebookModel.FeedData> getProfileFeed(string accesstoken)
        {
            this.facebookEndpoint = new facebookEndpoint(accesstoken);

            client.endpoint = facebookEndpoint.getByProfileFeedEndpoint();
            string response = client.makeRequest(httpVerb.GET);

            JsonParser<facebookModel> JsonParser = new JsonParser<facebookModel>();

            facebookModel deserialisedfacebookmodel = new facebookModel();
            deserialisedfacebookmodel = JsonParser.parseJson(response);

            List<facebookModel.FeedData> feedDatas = new List<facebookModel.FeedData>();

            foreach (facebookModel.FeedData feed in deserialisedfacebookmodel.data)
            {
                feedDatas.Add(feed);
            }

            return feedDatas;
        }

        /*Posts*/
        [HttpGet]
        [Route("getProfilePosts")]
        public List<facebookModel.FeedData> getProfilePosts(string accesstoken)
        {
            this.facebookEndpoint = new facebookEndpoint(accesstoken);

            client.endpoint = facebookEndpoint.getByProfilePostsEndpoint();
            string response = client.makeRequest(httpVerb.GET);

            JsonParser<facebookModel> JsonParser = new JsonParser<facebookModel>();

            facebookModel deserialisedfacebookmodel = new facebookModel();
            deserialisedfacebookmodel = JsonParser.parseJson(response);

            List<facebookModel.FeedData> feedDatas = new List<facebookModel.FeedData>();

            foreach (facebookModel.FeedData feed in deserialisedfacebookmodel.data)
            {
                feedDatas.Add(feed);
            }

            return feedDatas;
        }

        /*Likes*/
        [HttpGet]
        [Route("getProfileLikes")]
        public List<facebookLikesModel.LikesData> getProfileLikes(string accesstoken)
        {
            this.facebookEndpoint = new facebookEndpoint(accesstoken);

            client.endpoint = facebookEndpoint.getByProfileLikesEndpoint();
            string response = client.makeRequest(httpVerb.GET);

            JsonParser<facebookLikesModel> JsonParser = new JsonParser<facebookLikesModel>();

            facebookLikesModel deserialisedfacebookmodel = new facebookLikesModel();
            deserialisedfacebookmodel = JsonParser.parseJson(response);

            List<facebookLikesModel.LikesData> feedDatas = new List<facebookLikesModel.LikesData>();

            foreach (facebookLikesModel.LikesData feed in deserialisedfacebookmodel.data)
            {
                feedDatas.Add(feed);
            }

            return feedDatas;
        }

        /*Photos*/
        [HttpGet]
        [Route("getProfilePhotos")]
        public List<facebookPhotosModel.PhotoData> getProfilePhotos(string accesstoken)
        {
            this.facebookEndpoint = new facebookEndpoint(accesstoken);

            client.endpoint = facebookEndpoint.getByProfilePhotosEndpoint();
            string response = client.makeRequest(httpVerb.GET);

            JsonParser<facebookPhotosModel> JsonParser = new JsonParser<facebookPhotosModel>();

            facebookPhotosModel deserialisedfacebookmodel = new facebookPhotosModel();
            deserialisedfacebookmodel = JsonParser.parseJson(response);

            List<facebookPhotosModel.PhotoData> PhotoData = new List<facebookPhotosModel.PhotoData>();

            foreach (facebookPhotosModel.PhotoData feed in deserialisedfacebookmodel.data)
            {
                PhotoData.Add(feed);
            }

            return PhotoData;
        }

        /*Comment - User ID*/
        [HttpGet]
        [Route("UserId")]
        public List<DataUserId> GetUserId(string accesstoken)
        {
            List<DataUserId> facebookPost = new List<DataUserId>();
            this.facebookEndpoint = new facebookEndpoint(accesstoken);
            client.endpoint = facebookEndpoint.getUserIdForPost();
            string response = client.makeRequest(httpVerb.GET);

            JsonParser<facebookPostCommentModel> JsonParser = new JsonParser<facebookPostCommentModel>();
            facebookPostCommentModel deserialisedfacebookmodel = new facebookPostCommentModel();
            deserialisedfacebookmodel = JsonParser.parseJson(response);

            foreach (DataUserId posts in deserialisedfacebookmodel.data)
            {
                facebookPost.Add(posts);
            }

            return facebookPost;
        }

        /*Post ID*/
        [HttpGet]
        [Route("PostId")]
        public List<DataPagePost> GetPostId(string id, string accesstoken)
        {
            List<DataPagePost> facebookPost = new List<DataPagePost>();
            this.facebookEndpoint = new facebookEndpoint(accesstoken);
            client.endpoint = facebookEndpoint.getPagePost(id);
            string response = client.makeRequest(httpVerb.GET);

            JsonParser<facebookPageModel> JsonParser = new JsonParser<facebookPageModel>();
            facebookPageModel deserialisedfacebookmodel = new facebookPageModel();
            deserialisedfacebookmodel = JsonParser.parseJson(response);

            foreach(DataPagePost posts in deserialisedfacebookmodel.data)
            {
                facebookPost.Add(posts);
            }
            return facebookPost;
        }

        /*Post Comment*/
        [HttpPost]
        [Route("PostComment")]
        public IHttpActionResult postComment(string id, string message, string access_token, string accesstoken)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            this.facebookEndpoint = new facebookEndpoint(accesstoken);
            client.endpoint = facebookEndpoint.postComment(id, message, access_token);
            string response = client.makeRequest(httpVerb.POST);

            return Ok();
        }
    }
}
