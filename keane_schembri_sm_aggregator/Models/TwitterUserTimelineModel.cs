﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace keane_schembri_sm_aggregator.Models
{
    public class TwitterUserTimelineModel
    {
        public string created_at { get; set; }
        public string id { get; set; }
        public string id_str { get; set; }
        public string text { get; set; }
    }
}