﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace keane_schembri_sm_aggregator.Models
{
    public class TwitterFollowingModel
    {
        public List<UserData> users { get; set; }

        public class UserData
        {
            public string name { get; set; } 
        }
    }
}