﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace keane_schembri_sm_aggregator.Models
{
    public class facebookPageModel
    {
        public List<DataPagePost> data { get; set; }
        public class DataPagePost
        {
            public string created_time { get; set; }
            public string message { get; set; }
            public string id { get; set; }
        }
    }
}