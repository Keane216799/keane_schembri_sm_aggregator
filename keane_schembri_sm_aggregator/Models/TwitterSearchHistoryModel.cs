﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace keane_schembri_sm_aggregator.Models
{
    public class TwitterSearchHistoryModel
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int searchHistory_id { get; set; }
        public string searchQuery { get; set; }
    }
}