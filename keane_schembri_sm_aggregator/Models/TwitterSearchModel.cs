﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace keane_schembri_sm_aggregator.Models
{
    public class TwitterSearchModel
    {
        public List<search> statuses { get; set; }
        
        public class search
        {
            public string text { get; set; }
        }
    }
}