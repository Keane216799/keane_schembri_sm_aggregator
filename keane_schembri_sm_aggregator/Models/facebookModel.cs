﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace keane_schembri_sm_aggregator.Models
{
    public class facebookModel
    {
        public string id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string birthday { get; set; }
        public string gender { get; set; }

        public List<FeedData> data { get; set; }

        public class FeedData
        {
            public string message { get; set; }
            public string created_time { get; set;}
            public string id { get; set; }
        }
    }
}