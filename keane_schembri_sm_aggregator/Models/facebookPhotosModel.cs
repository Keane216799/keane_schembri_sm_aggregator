﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace keane_schembri_sm_aggregator.Models
{
    public class facebookPhotosModel
    {
        public List<PhotoData> data { get; set; }

        public class PhotoData
        {
            public string picture { get; set; }
            public string id { get; set; }
        }
    }
}