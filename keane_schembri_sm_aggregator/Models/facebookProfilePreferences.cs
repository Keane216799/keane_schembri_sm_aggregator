﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace keane_schembri_sm_aggregator.Models
{
    public class facebookProfilePreferences
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int preference_id { get; set; }
        public Boolean name { get; set; }
        public Boolean email { get; set; }
        public Boolean birthday { get; set; }
        public Boolean gender { get; set; }
    }
}