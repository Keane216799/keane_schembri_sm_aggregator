﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace keane_schembri_sm_aggregator.Models
{
    public class facebookLikesModel
    {
        public List<LikesData> data { get; set; }

        public class LikesData
        {
            public string name { get; set; }
            public string id { get; set; }
            public string created_time { get; set; }
        }
    }
}