﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace keane_schembri_sm_aggregator.Models
{
    public class facebookPostCommentModel
    {
        public List<DataUserId> data { get; set; }
        public class DataUserId
        {
            public string access_token { get; set; }
            public string name { get; set; }
            public string id { get; set; }
        }
    }
}