﻿using keane_schembri_sm_aggregator.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace keane_schembri_sm_aggregator.Endpoints
{
    class facebookEndpoint : Endpoint
    {
        public string accesstoken;

        public facebookEndpoint(string accesstoken) : base(
            "https://graph.facebook.com/v7.0/me")
        {
            this.accesstoken = accesstoken;
        }

        public string getByNameEndpoint()
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint);
            stringBuilder.Append("?fields=");
            stringBuilder.Append("name&access_token=");
            stringBuilder.Append(accesstoken);
            return stringBuilder.ToString();
        }

        public string getByProfileEndpoint()
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint);
            stringBuilder.Append("?fields=");
            stringBuilder.Append("id,name,email,birthday,gender");
            stringBuilder.Append("&access_token=");
            stringBuilder.Append(accesstoken);
            return stringBuilder.ToString();
        }

        /*
        "https://graph.facebook.com/v7.0/me/feed?access_token=" 
        */
        public string getByProfileFeedEndpoint()
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint);
            stringBuilder.Append("/feed?");
            stringBuilder.Append("access_token=");
            stringBuilder.Append(accesstoken);
            return stringBuilder.ToString();
        }
        /*
        "https://graph.facebook.com/v7.0/me/feed?access_token=" 
        */
        public string getByProfilePostsEndpoint()
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint);
            stringBuilder.Append("/posts?");
            stringBuilder.Append("access_token=");
            stringBuilder.Append(accesstoken);
            return stringBuilder.ToString();
        }
        /*
        https://graph.facebook.com/v7.0/me/likes?access_token= 
        */
        public string getByProfileLikesEndpoint()
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint);
            stringBuilder.Append("/likes?");
            stringBuilder.Append("access_token=");
            stringBuilder.Append(accesstoken);
            return stringBuilder.ToString();
        }
        /*
        https://graph.facebook.com/v7.0/me/photos?fields=picture&type=uploaded&access_token= 
        */
        public string getByProfilePhotosEndpoint()
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint);
            stringBuilder.Append("/photos?fields=picture&type=uploaded");
            stringBuilder.Append("&access_token=");
            stringBuilder.Append(accesstoken);
            return stringBuilder.ToString();
        }

        public string getUserIdForPost()
        {
            StringBuilder stringBuilder = new StringBuilder(baseEndpoint);
            stringBuilder.Append("/accounts?");
            stringBuilder.Append("access_token=");
            stringBuilder.Append(accesstoken);
            return stringBuilder.ToString();
        }

        public string getPagePost(string id)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("https://graph.facebook.com/v7.0/");
            stringBuilder.Append(id);
            stringBuilder.Append("/posts?");
            stringBuilder.Append("access_token=");
            stringBuilder.Append(accesstoken);
            return stringBuilder.ToString();
        }

        public string postComment(string id, string message, string access_token)
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.Append("https://graph.facebook.com/v7.0/");
            stringBuilder.Append(id);
            stringBuilder.Append("/comments?");
            stringBuilder.Append("message=");
            stringBuilder.Append(message);
            stringBuilder.Append("&access_token=");
            stringBuilder.Append(access_token);
            return stringBuilder.ToString();
        }
    }
}