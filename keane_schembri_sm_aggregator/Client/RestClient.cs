﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;

namespace keane_schembri_sm_aggregator.Client
{
    public enum httpVerb
    {
        GET,
        POST
    }

    public class RestClient
    {
        public string endpoint { get; set; }
        public httpVerb httpMethod { get; set; }

        public RestClient()
        {
            endpoint = string.Empty;
            //httpMethod = httpVerb.GET;
        }

        //this method is going to send the request and return the json result (if found)
        public string makeRequest(httpVerb httpMethod)
        {
            string strResponseValue = string.Empty;

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(endpoint);

            request.Method = httpMethod.ToString();

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    throw new ApplicationException($"Error Code: {response.StatusCode}");
                }

                using (Stream responseStream = response.GetResponseStream())
                {
                    if (responseStream != null)
                    {
                        using (StreamReader reader = new StreamReader(responseStream))
                        {
                            strResponseValue = reader.ReadToEnd();
                        }
                    }
                }

            }

            return strResponseValue;
        }

    }
}