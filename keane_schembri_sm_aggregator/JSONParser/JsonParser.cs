﻿using keane_schembri_sm_aggregator.Models;
using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web;

namespace keane_schembri_sm_aggregator.JSONParser
{
    public class JsonParser<T> : IDisposable
    {
        bool disposed = false;
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;
            if (disposing)
            {
                handle.Dispose();
            }

            disposed = true;
        }

        public T parseJson(string json)
        {
            var deserializedModel = Activator.CreateInstance(typeof(T));

            
            var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(json));
            var serializer = new DataContractJsonSerializer(typeof(T));
            deserializedModel = serializer.ReadObject(memoryStream);
                    

            return (T)deserializedModel;
        }
    }
}